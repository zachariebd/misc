

import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
from osgeo import osr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
from xml.dom import minidom
from scipy.stats import mstats
import shutil
from pyproj import Proj, transform
import glob



def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise


def makeTreeCover():
    f_tree = "/work/OT/siaa/Theia/Neige/CoSIMS/data/tree_cover_density/original_tiling/TCD_2015_020m_eu_03035_d05_full.tif"
   
    tree = gdal.Open(f_tree)
    epsg_multi = (gdal.Info(multi, format='json')['coordinateSystem']['wkt'].rsplit('"EPSG","', 1)[-1].split('"')[0])
    gdal.Warp(dir_inputs_date + "/tree_cover.tif",tree,format= 'GTiff',dstSRS="EPSG:" + epsg_multi,resampleAlg="near",xRes= res,yRes= res, outputBounds=[minx, miny, maxx, maxy])




def makeMaskFromMS():
    # make nodata mask 
    cond = " '(im1b1<0 || im1b1>1) ? 0:1' "
    os.system("otbcli_BandMath -il " + f_multi + " -exp " + cond + " -out " + os.path.join(dir_inputs_mask,"tifmask_ND.tif"))
    l_masks.append(os.path.join(dir_inputs_mask,"tifmask_ND.tif"))




def makeMasksFromSHP():
    
    for f in os.listdir(dir_inputs_mask) :
        if (".shp" in f) :
            shpname = os.path.join(dir_inputs_mask,f)
            maskname = "tifmask_" + os.path.splitext(f)[0] + ".tif"
            os.system("gdal_rasterize -te " + str(minx) + " " + str(miny) + " " + str(maxx)+ " " + str(maxy) + " -tr " + str(res) + " " + str(res) + "  -burn 0 -init 1  " + shpname + " " + os.path.join(dir_inputs_mask,maskname))
            l_masks.append(os.path.join(dir_inputs_mask,maskname))



def preProcess():


    os.system("otbcli_RadiometricIndices -in " + f_multi + " -channels.red 1 -channels.green 2 -channels.blue 3 -channels.nir 4  -list Vegetation:NDVI -out " + dir_outputs_date + "/RadInd.tif")
    os.system("otbcli_ConcatenateImages -il " + f_multi + " " + dir_outputs_date + "/RadInd.tif" + " -out " + dir_outputs_date + "/im4classif_radio.tif")
    os.system("otbcli_ManageNoData -in "+dir_outputs_date+"/im4classif_radio.tif"+" -mode changevalue -mode.changevalue.newv -9999 -out " + dir_outputs_date + "/im4classif_radio.tif")
    
    for mask in l_masks:
        os.system("otbcli_ManageNoData -in "+dir_outputs_date+"/im4classif_radio.tif"+" -mode apply -mode.apply.mask " + mask + " -mode.apply.ndval -9999 -out " + dir_outputs_date + "/im4classif_radio.tif")


def trainModel(trainSHP,name):

    f_radio = ""

    for f in os.listdir(dir_outputs_date) :
        if ("im4classif_radio.tif" in f) :
            f_radio = os.path.join(dir_outputs_date,f)

    f_class = os.path.join(dir_outputs_date,name + "_class.tif")
    f_class_snw = os.path.join(dir_outputs_date,name + "_class_snw.tif")        
            
    os.system("otbcli_PolygonClassStatistics  -in "+f_radio+" -vec "+trainSHP+" -field id -out "+ dir_outputs_date+"/classes.xml")
    os.system("otbcli_SampleSelection -in  "+f_radio+" -vec "+trainSHP+" -instats "+ dir_outputs_date+"/classes.xml"+ " -field id -strategy all -outrates "+ dir_outputs_date+"/rates.csv" + " -out "+ dir_outputs_date+"/samples.sqlite")
    os.system("otbcli_SampleExtraction -in "+f_radio+" -vec "+ dir_outputs_date+"/samples.sqlite" + " -outfield prefix -outfield.prefix.name band_ -field id")
    os.system("otbcli_TrainVectorClassifier -io.vd "+ dir_outputs_date+"/samples.sqlite" + "  -cfield id -io.out "+ dir_outputs_date+"/model.rf" + " -classifier sharkrf -feat band_0 band_1 band_2 band_3 band_4")
    os.system("otbcli_ImageClassifier -in "+f_radio+" -model  "+ dir_outputs_date+"/model.rf" + " -out "+ f_class + " -nodatalabel -9999")

    for mask in l_masks:
        os.system("otbcli_ManageNoData -in "+ f_class + " -out  "+ f_class + " -mode apply -mode.apply.mask "+ mask)

    cond = " '((im1b1+im1b2+im1b3)/3>2000) ? 1 : im2b1' "
    os.system("otbcli_BandMath -il "+f_radio+" "+ f_class + " -out "+ f_class_snw + " -exp " + cond)




def testModel(testSHP,name):


    f_class_snw = os.path.join(dir_outputs_date,name + "_class_snw.tif")
    f_confm = os.path.join(dir_outputs_date,name + "_confm.tif")

    os.system("otbcli_ComputeConfusionMatrix -in  " +f_class_snw+" -ref  vector  -ref.vector.in "+testSHP+" -ref.vector.field id -out  "+f_confm)





source = "PLEIADES"
date = "20160707"


dir_class = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/snowclass/data"

dir_inputs = dir_class + "/INPUTS/" + source 
dir_outputs = dir_class + "/OUTPUTS/" + source 

dir_inputs_date = dir_inputs + "/" + date
dir_outputs_date = dir_outputs + "/" + date

dir_inputs_mask = dir_inputs_date + "/" + "MASKS"
dir_inputs_train = dir_inputs_date + "/" + "TRAIN"
mkdir_p(dir_outputs_date)
f_multi = ""
f_train_shp = ""
l_masks = []

for f in os.listdir(dir_inputs_date) :
    if ("MS" in f) :
        f_multi = os.path.join(dir_inputs_date,f)
for f in os.listdir(dir_inputs_train) :
    if (".shp" in f) :
        f_train_shp = os.path.join(dir_inputs_train,f)
multi = gdal.Open(f_multi)
GT = multi.GetGeoTransform()
minx = GT[0]
maxy = GT[3]
maxx = minx + GT[1] * multi.RasterXSize
miny = maxy + GT[5] * multi.RasterYSize
res = GT[1]   




makeMaskFromMS()
makeMasksFromSHP()
preProcess()
trainModel(f_train_shp, date + "_zac")
#testModel(f_test_shp, date + "_zac")
