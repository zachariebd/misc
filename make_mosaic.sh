#!/bin/bash

module load gdal_openjpeg

a=2019
m=04
d=01

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif


a=2019
m=05
d=28

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif

a=2019
m=06
d=30

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif

a=2019
m=07
d=20

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif


a=2019
m=08
d=04

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif

a=2019
m=10
d=03

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif

a=2019
m=11
d=17

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif


a=2019
m=12
d=24

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif

a=2020
m=02
d=10

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif

a=2020
m=03
d=18

p=$(find /datalake/S2-L1C/31TCH/$a/$m/$d/ -type d -name IMG_DATA)
gdal_merge.py -o alcd/${a}${m}${d}comp.tif -separate $p/*B03.jp2 $p/*B10.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 200 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}comp.tif alcd/comp/${a}-${m}-${d}.tif
gdal_merge.py -o alcd/${a}${m}${d}rgb.tif -separate $p/*B04.jp2 $p/*B03.jp2 $p/*B02.jp2
gdal_translate -scale_1 0 4000 0 255 -scale_2 0 4000 0 255 -scale_3 0 4000 0 255 -ot Byte -co PHOTOMETRIC=RGB alcd/${a}${m}${d}rgb.tif alcd/rgb/${a}-${m}-${d}.tif




# une fois que tu as les compo colorées
montage -tile x1 -geometry 400x+5+5 -label %t -pointsize 30 alcd/comp/* monMontageComp.jpg
montage -tile x1 -geometry 400x+5+5 -label %t -pointsize 30 alcd/rgb/* monMontageRGB.jpg
