#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
from osgeo import osr, ogr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import shutil
from pyproj import Proj, transform
import glob
import random
import csv
from math import sqrt
import json


def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise

datasets_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/DATASETS"
mountain_path =os.path.join(datasets_path,"SUPPORT_DATA/DATA/mountains/m_massifs_v1_WGS84.shp")
images_path = os.path.join(datasets_path,"IMAGES")
satellite_path = os.path.join(images_path,"LANDSAT/TEST")
out_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/usgs_download/coverage_output"
mkdir_p(out_path)
os.system("rm -f "+os.path.join(out_path,"*"))
where_burn= "\"m_massive='pyr'\"" 

xres_platform = 0
yres_platform = 0
if len(os.listdir(satellite_path)) == 0 : exit()

for location in os.listdir(satellite_path):
    print(location)
    if len(os.listdir(os.path.join(satellite_path,location))) == 0 : continue
    for collection in os.listdir(os.path.join(satellite_path,location)):
        print(collection)
        xmin_collection= 0
        xmax_collection= 0
        ymax_collection= 0
        ymin_collection= 0

        if len(os.listdir(os.path.join(satellite_path,location,collection))) == 0 : continue
        for platform in os.listdir(os.path.join(satellite_path,location,collection)):
            print(platform)
            platform_path = os.path.join(satellite_path,location,collection,platform)
            searchcsv = os.path.join(platform_path,"search.csv")
            if os.path.exists(searchcsv) is False: continue
            platform_name = location+"_"+collection+"_"+platform
            driver_shp = ogr.GetDriverByName("ESRI Shapefile")
            ds_shp= driver_shp.CreateDataSource(os.path.join(out_path,platform_name +".shp"))
            srs_shp = osr.SpatialReference()
            srs_shp.ImportFromEPSG(4326)
            layer= ds_shp.CreateLayer(platform_name, srs_shp, ogr.wkbPolygon)
            layer.CreateField(ogr.FieldDefn("count", ogr.OFTInteger))
            xmin_platform= 0
            xmax_platform= 0
            ymax_platform= 0
            ymin_platform= 0
            xres_platform = 0
            yres_platform = 0
            nb_platform = 0
            with open(searchcsv) as search_file:
                csvReader = csv.DictReader(search_file)
                i = 0
                for product in csvReader:
                    size_x = int(int(product["Reflective Samples"]))
                    size_y = int(int(product["Reflective Lines"]))
                    #resolution = product["properties"]["resolution"]
                    date = product["Acquisition Date"]
                    #print(date)
                    
                    upperl = [float(product["UL Corner Long dec"]),float(product["UL Corner Lat dec"])]
                    upperr = [float(product["UR Corner Long dec"]),float(product["UR Corner Lat dec"])]
                    lowerr = [float(product["LR Corner Long dec"]),float(product["LR Corner Lat dec"])]
                    lowerl = [float(product["LL Corner Long dec"]),float(product["LL Corner Lat dec"])]
                    xmin = min(lowerl[0],upperl[0])
                    xmax = max(lowerr[0],upperr[0])
                    ymax = max(upperl[1],upperr[1],lowerr[1],lowerl[1])
                    ymin = min(upperl[1],upperr[1],lowerr[1],lowerl[1])
                    xres_platform = float(xmax - xmin)/float(size_x)
                    yres_platform = float(ymax - ymin)/float(size_y)
                    #if xres_platform not in xres: xres.append(xres_platform)
                    #if yres_platform not in xres: yres.append(xres_platform)
                    if i == 0:
                        xmin_platform= xmin
                        xmax_platform= xmax
                        ymax_platform= ymax
                        ymin_platform= ymin
                    else:
                        if xmin < xmin_platform: xmin_platform = xmin
                        if xmax > xmax_platform: xmax_platform = xmax
                        if ymax > ymax_platform: ymax_platform = ymax
                        if ymin < ymin_platform: ymin_platform = ymin
                    i = i + 1


                    
                    ## add features to shapefile

                    feature = ogr.Feature(layer.GetLayerDefn())
                    feature.SetField("count", 1)
                    wkt = "POLYGON((%f %f,%f %f,%f %f,%f %f))" %  (upperl[0], upperl[1],upperr[0], upperr[1],lowerr[0], lowerr[1],lowerl[0], lowerl[1])
                    poly = ogr.CreateGeometryFromWkt(wkt)
                    feature.SetGeometry(poly)
                    layer.CreateFeature(feature)
                    feature = None
                    
                    
            xsize_platform = int(float(xmax_platform - xmin_platform)/(xres_platform*5))
            ysize_platform = int(float(ymax_platform - ymin_platform)/(yres_platform*5))

            ds_shp = None
            os.system("rm -f "+ os.path.join(out_path,platform_name+".tif"))
            fileformat = "GTiff"
            driver = gdal.GetDriverByName(fileformat)
            dst_ds = driver.Create(os.path.join(out_path,platform_name+".tif"),xsize=xsize_platform,ysize=ysize_platform,bands=1,eType=gdal.GDT_UInt16)
            dst_ds.SetGeoTransform([xmin_platform,xres_platform*5,0,ymax_platform,0,-1*yres_platform*5])
            srs = osr.SpatialReference()       
            srs.ImportFromEPSG(4326) 
            dst_ds.SetProjection(srs.ExportToWkt()) 
            raster = numpy.zeros((ysize_platform, xsize_platform), dtype=numpy.uint16)
            dst_ds.GetRasterBand(1).WriteArray(raster)
            dst_ds = None
            os.system("gdal_rasterize  -burn 1  -add " + os.path.join(out_path,platform_name +".shp")+ " " + os.path.join(out_path,platform_name+".tif"))
            
            if nb_platform == 0:
                xmin_collection= xmin_platform
                xmax_collection= xmax_platform
                ymax_collection= ymax_platform
                ymin_collection= ymin_platform
            else:
                if xmin_platform < xmin_collection: xmin_collection = xmin_platform
                if xmax_platform > xmax_collection: xmax_collection = xmax_platform
                if ymax_platform > ymax_collection: ymax_collection = ymax_platform
                if ymin_platform < ymin_collection: ymin_collection = ymin_platform
            nb_platform = nb_platform + 1
        
        
        xsize_collection = int(float(xmax_collection - xmin_collection)/(xres_platform*5))
        ysize_collection = int(float(ymax_collection - ymin_collection)/(yres_platform*5))     
        collection_name = location+"_"+collection
        os.system("rm -f "+ os.path.join(out_path,collection_name+".tif"))
        fileformat = "GTiff"
        driver = gdal.GetDriverByName(fileformat)
        dst_ds = driver.Create(os.path.join(out_path,collection_name+".tif"),xsize=xsize_collection,ysize=ysize_collection,bands=1,eType=gdal.GDT_UInt16)
        dst_ds.SetGeoTransform([xmin_collection,xres_platform*5,0,ymax_collection,0,-1*yres_platform*5])
        srs = osr.SpatialReference()       
        srs.ImportFromEPSG(4326) 
        dst_ds.SetProjection(srs.ExportToWkt()) 
        raster = numpy.zeros((ysize_collection, xsize_collection), dtype=numpy.uint16)
        dst_ds.GetRasterBand(1).WriteArray(raster)
        dst_ds = None
        for col in os.listdir(out_path):
            if collection_name in col and ".shp" in col: os.system("gdal_rasterize  -burn 1  -add " + os.path.join(out_path,col)+ " " + os.path.join(out_path,collection_name+".tif"))

