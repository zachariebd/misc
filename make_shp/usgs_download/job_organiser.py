
import sys
import os
import errno
import re
import copy
from datetime import datetime, timedelta, date
import glob
import argparse
import logging
import subprocess


def call_subprocess(process_list):
    """ Run subprocess and write to stdout and stderr
    """
    logging.info("Running: " + " ".join(process_list))
    process = subprocess.Popen(
        process_list,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out, err = process.communicate()
    
    logging.info(out)
    sys.stderr.write(str(err))
    return out




def makeJobs(params,sh_script,jobname):
    
    command_A = ["qsub",
                       "-V",
                       "-N",
                       jobname,
                       "-j",
                       "oe",
                       "-o",
                       "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/usgs_download/logs",
                       "-e",
                       "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/usgs_download/logs",
                       "-v",
                       params]
                       
                       
    command_Out = [sh_script]
    command_J = command_A + command_Out
    job_id = call_subprocess(command_J)
   
        
    return job_id



