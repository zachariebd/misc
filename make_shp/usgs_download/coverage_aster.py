#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
from osgeo import osr, ogr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import shutil
from pyproj import Proj, transform
import glob
import random
import csv
from math import sqrt
import json
import itertools


def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise

def getDateFromStr(N):
    sepList = ["","-","_","/"]
    date = ''
    for s in sepList :
        found = re.search('\d{4}'+ s +'\d{2}'+ s +'\d{2}', N)
        if found != None :
           date = datetime.strptime(found.group(0), '%Y'+ s +'%m'+ s +'%d').date()
           break
    return date

datasets_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/DATASETS"
mountain_path =os.path.join(datasets_path,"SUPPORT_DATA/DATA/grid_mountains/grid_mountains.shp")
images_path = os.path.join(datasets_path,"IMAGES")
satellite_path = os.path.join(images_path,"TERRA/SHP")
out_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/OUTPUTS/get_products/usgs_download/TERRA/"
mkdir_p(out_path)
os.system("rm -f "+os.path.join(out_path,"*"))


periods = ["1980-01-01","1990-01-01","2000-01-01","2010-01-01","2020-01-01"]


if len(os.listdir(satellite_path)) == 0 : exit()

satellite_data = {}

srs =  osr.SpatialReference()
srs.ImportFromEPSG(4326)
drv = ogr.GetDriverByName( 'ESRI Shapefile' )

for instruments in os.listdir(satellite_path):
    print(instruments)
    
    instrument_shp = glob.glob(os.path.join(satellite_path,instruments,'*.shp'))[0]
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(instrument_shp, 0)
    layer = dataSource.GetLayer()
    sensor = ""
    #premiere lecture des polygons
    print("first parsing loop")
    for feature in layer:
        geom = feature.GetGeometryRef().ExportToWkt()
        date = getDateFromStr(feature.GetField("Acquisitio")).strftime("%Y%m%d")
        SWIR_MODE = feature.GetField("SWIR MODE")
 
        WRS_Path = int(feature.GetField("WRS Path"))
        WRS_Row = int(feature.GetField("WRS Row"))
        if SWIR_MODE == "ON": 
            sensor = "ASTER_VNIR_SWIR"
        else: 
            sensor = "ASTER_VNIR"
        if sensor not in satellite_data : 
            satellite_data[sensor] = {}
            print("adding sensor :" , sensor)
        if date not in satellite_data[sensor] : 
            satellite_data[sensor][date] = []
            print("adding date :" , date)
        if geom not in satellite_data[sensor][date]: satellite_data[sensor][date].append(geom)
        
    print("end first parsing loop")


print("start merging loop")
#decoupage des polygons qui se chevauchent
for sensor in satellite_data :
    for date in satellite_data[sensor] :
        #print("geom_c",satellite_data[sensor][date][0])
        geom_c= ogr.CreateGeometryFromWkt(satellite_data[sensor][date][0])
        print(geom_c)
        print("length",len(satellite_data[sensor][date])) 
        if len(satellite_data[sensor][date]) >= 2 : 
            for geom_a in satellite_data[sensor][date][1:]:
                print("geom_a:",geom_a)
                geom_c = ogr.Geometry.Union(geom_c,ogr.CreateGeometryFromWkt(geom_a))      
        #print("add geom_c")
        satellite_data[sensor][date] = ""
        satellite_data[sensor][date] = geom_c.ExportToWkt()
        # for WRS_Row in satellite_data[sensor][date][WRS_Path] :
            # if WRS_Row - 1 in satellite_data[sensor][date][WRS_Path]:
                # print("start clipping with row : ",WRS_Row - 1)
                # geom_c = ogr.CreateGeometryFromWkt(satellite_data[sensor][date][WRS_Path][WRS_Row])
                # geom_a = ogr.CreateGeometryFromWkt(satellite_data[sensor][date][WRS_Path][WRS_Row-1])
                # geom_diff = ogr.Geometry.Difference(geom_c,geom_a)
                # geom_out = geom_diff
                # print(geom_diff.GetGeometryName())
                # area_max = 0
                # i_max = 0
                # if geom_diff.GetGeometryName() == "MULTIPOLYGON":
                    # for i in range(0, geom_diff.GetGeometryCount()):
                        # g = geom_diff.GetGeometryRef(i)
                        # area = g.GetArea()
                        # if area > area_max: 
                            # area_max = area
                            # i_max = i
                    # g_max = geom_diff.GetGeometryRef(i_max)
                    # geom_out = ogr.CreateGeometryFromWkt(g_max.ExportToWkt())    
                # satellite_data[sensor][date][WRS_Path][WRS_Row] = geom_out.ExportToWkt()
                # print("end clipping")












                        
                        
for p in range(len(periods)-1) :
    min_date = periods[p]
    max_date = periods[p+1]
    for sensor in satellite_data:
        count = 0
        print("creating output shapefile")
        ds_sensor = drv.CreateDataSource(os.path.join(out_path,sensor+"_"+min_date+"_"+max_date+".shp"))
        layer_sensor = ds_sensor.CreateLayer(sensor+"_"+min_date+"_"+max_date, srs, ogr.wkbPolygon)
        date_field = ogr.FieldDefn("Date", ogr.OFTString)
        sensor_field = ogr.FieldDefn("Sensor", ogr.OFTString)
        count_field = ogr.FieldDefn("Count", ogr.OFTInteger)
        layer_sensor.CreateField(date_field)
        layer_sensor.CreateField(count_field)
        layer_sensor.CreateField(sensor_field)
        for date in satellite_data[sensor]:
            #print(date)
            if  getDateFromStr(date) < getDateFromStr(min_date) or getDateFromStr(date) >= getDateFromStr(max_date): continue
            count = count + 1
            wkt = satellite_data[sensor][date]
            #print("add polygon in output shapefile")
            featureDefn_sensor = layer_sensor.GetLayerDefn()
            feature_sensor = ogr.Feature(featureDefn_sensor)
            #print("setgeom")
            print(wkt)
            print("toto")
            geom_out = ogr.CreateGeometryFromWkt(wkt)
            feature_sensor.SetGeometry(geom_out)
            #print("setfields")
            feature_sensor.SetField("Sensor", sensor)
            feature_sensor.SetField("Date", date)
            feature_sensor.SetField("Count", 1)
            #print("add feature")
            layer_sensor.CreateFeature(feature_sensor)
            #print("polygon is added")
        ds_sensor = None
        if count == 0 : os.system("rm "+os.path.join(out_path,sensor+"_"+min_date+"_"+max_date+".*"))
           
           
  
