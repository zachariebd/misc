#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
from osgeo import osr, ogr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import shutil
from pyproj import Proj, transform
import glob
import random

from math import sqrt
import json


def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise
            
def getDateFromStr(N):
    sepList = ["","-","_","/"]
    date = ''
    for s in sepList :
        found = re.search('\d{4}'+ s +'\d{2}'+ s +'\d{2}', N)
        if found != None :
           date = datetime.strptime(found.group(0), '%Y'+ s +'%m'+ s +'%d').date()
           break
    return date

datasets_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/DATASETS"
mountain_path =os.path.join(datasets_path,"SUPPORT_DATA/DATA/grid_mountains/grid_mountains.shp")
images_path = os.path.join(datasets_path,"IMAGES")
satellite_path = os.path.join(images_path,"SPOT/MTD")
out_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/OUTPUTS/get_products/theia_download/SPOT/"
mkdir_p(out_path)
os.system("rm -f "+os.path.join(out_path,"*"))


#periods = ["1980-01-01","1985-01-01","1990-01-01","1995-01-01","2000-01-01","2005-01-01","2010-01-01","2015-01-01","2020-01-01"]
periods = ["1980-01-01","1990-01-01","2000-01-01","2010-01-01","2020-01-01"]
satellite_data = {}

srs =  osr.SpatialReference()
srs.ImportFromEPSG(4326)
drv = ogr.GetDriverByName( 'ESRI Shapefile' )

if len(os.listdir(satellite_path)) == 0 : exit()
product_list = []
print("first parsing loop")
for location in os.listdir(satellite_path):
    print(location)
    if len(os.listdir(os.path.join(satellite_path,location))) == 0 : continue
    for collection in os.listdir(os.path.join(satellite_path,location)):
        print(collection)
        if len(os.listdir(os.path.join(satellite_path,location,collection))) == 0 : continue
        for platform in os.listdir(os.path.join(satellite_path,location,collection)):
            print(platform)
            if len(os.listdir(os.path.join(satellite_path,location,collection,platform))) == 0 : continue
            for period2 in os.listdir(os.path.join(satellite_path,location,collection,platform)):
                if len(os.listdir(os.path.join(satellite_path,location,collection,platform,period2))) == 0 : continue
                searchjson = os.path.join(os.path.join(satellite_path,location,collection,platform,period2),"search.json")
                if os.path.exists(searchjson) is False: continue
                with open(searchjson) as search_file:
                    data = json.load(search_file)
                    if len(data["features"]) == 0 : continue
                    for i in range(len(data["features"])):
                        product = data["features"][i]
                        ID = product["properties"]["productIdentifier"]
                        if ID not in product_list:
                            product_list.append(ID)
                        else:
                            continue
                        date = getDateFromStr(product["properties"]["startDate"]).strftime("%Y%m%d")
                        coordinates = product["geometry"]["coordinates"] #SWH : [[[minx,maxy],[maxx,maxy],[maxx,miny],[minx,miny],[minx,maxy]]]
                        upperl = coordinates[0][0]
                        upperr = coordinates[0][1]
                        lowerr = coordinates[0][2]
                        lowerl = coordinates[0][3]
                        wkt = "POLYGON((%f %f,%f %f,%f %f,%f %f,%f %f))" %  (upperl[0], upperl[1],upperr[0], upperr[1],lowerr[0], lowerr[1],lowerl[0], lowerl[1],upperl[0], upperl[1])
                        sensor = product["properties"]["instrument"]
                        bands = product["properties"]["bands"]

                        if "HRVIR" in sensor: 
                            sensor = "HRVIR"
                        elif "HRV" in sensor : 
                            sensor = "HRV"
                        elif "HRG" in sensor:
                            sensor = "HRG"
                        else : 
                            continue
                        if "SWIR" in bands : sensor = sensor +"_"+"SWIR"
                            
                        if sensor not in satellite_data : 
                            satellite_data[sensor] = {}
                        if date not in satellite_data[sensor] : 
                            satellite_data[sensor][date] = []
                        if wkt not in satellite_data[sensor][date] : 
                            satellite_data[sensor][date].append(wkt)







print("start merging loop")
#decoupage des polygons qui se chevauchent
for sensor in satellite_data :
    for date in satellite_data[sensor] :
        #print("geom_c",satellite_data[sensor][date][0])
        geom_c= ogr.CreateGeometryFromWkt(satellite_data[sensor][date][0])
        print(geom_c)
        print("length",len(satellite_data[sensor][date])) 
        if len(satellite_data[sensor][date]) >= 2 : 
            for geom_a in satellite_data[sensor][date][1:]:
                print("geom_a:",geom_a)
                geom_c = ogr.Geometry.Union(geom_c,ogr.CreateGeometryFromWkt(geom_a))      
        #print("add geom_c")
        satellite_data[sensor][date] = ""
        satellite_data[sensor][date] = geom_c.ExportToWkt()
        # for WRS_Row in satellite_data[sensor][date][WRS_Path] :
            # if WRS_Row - 1 in satellite_data[sensor][date][WRS_Path]:
                # print("start clipping with row : ",WRS_Row - 1)
                # geom_c = ogr.CreateGeometryFromWkt(satellite_data[sensor][date][WRS_Path][WRS_Row])
                # geom_a = ogr.CreateGeometryFromWkt(satellite_data[sensor][date][WRS_Path][WRS_Row-1])
                # geom_diff = ogr.Geometry.Difference(geom_c,geom_a)
                # geom_out = geom_diff
                # print(geom_diff.GetGeometryName())
                # area_max = 0
                # i_max = 0
                # if geom_diff.GetGeometryName() == "MULTIPOLYGON":
                    # for i in range(0, geom_diff.GetGeometryCount()):
                        # g = geom_diff.GetGeometryRef(i)
                        # area = g.GetArea()
                        # if area > area_max: 
                            # area_max = area
                            # i_max = i
                    # g_max = geom_diff.GetGeometryRef(i_max)
                    # geom_out = ogr.CreateGeometryFromWkt(g_max.ExportToWkt())    
                # satellite_data[sensor][date][WRS_Path][WRS_Row] = geom_out.ExportToWkt()
                # print("end clipping")












                        
                        
for p in range(len(periods)-1) :
    min_date = periods[p]
    max_date = periods[p+1]
    print("creating output shapefiles for period:",periods[p],periods[p+1])
    for sensor in satellite_data:
        count = 0
        print("creating shapefile for sensor:",sensor)
        ds_sensor = drv.CreateDataSource(os.path.join(out_path,sensor+"_"+min_date+"_"+max_date+".shp"))
        layer_sensor = ds_sensor.CreateLayer(sensor+"_"+min_date+"_"+max_date, srs, ogr.wkbPolygon)
        date_field = ogr.FieldDefn("Date", ogr.OFTString)
        sensor_field = ogr.FieldDefn("Sensor", ogr.OFTString)
        count_field = ogr.FieldDefn("Count", ogr.OFTInteger)
        layer_sensor.CreateField(date_field)
        layer_sensor.CreateField(count_field)
        layer_sensor.CreateField(sensor_field)
        for date in satellite_data[sensor]:
            #print(date)
            if  getDateFromStr(date) < getDateFromStr(min_date) or getDateFromStr(date) >= getDateFromStr(max_date): continue
            count = count + 1
            wkt = satellite_data[sensor][date]
            #print("add polygon in output shapefile")
            featureDefn_sensor = layer_sensor.GetLayerDefn()
            feature_sensor = ogr.Feature(featureDefn_sensor)
            #print("setgeom")
            #print(wkt)
            #print("toto")
            geom_out = ogr.CreateGeometryFromWkt(wkt)
            feature_sensor.SetGeometry(geom_out)
            #print("setfields")
            feature_sensor.SetField("Sensor", sensor)
            feature_sensor.SetField("Date", date)
            feature_sensor.SetField("Count", 1)
            #print("add feature")
            layer_sensor.CreateFeature(feature_sensor)
            #print("polygon is added")
        print("saving output shapefile for sensor:",sensor,"with",count,"dates")
        ds_sensor = None
        print("finished output shapefile for sensor:",sensor)
        if count == 0 : os.system("rm "+os.path.join(out_path,sensor+"_"+min_date+"_"+max_date+".*"))
           
           
  
