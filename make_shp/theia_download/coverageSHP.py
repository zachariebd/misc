#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
from osgeo import osr, ogr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import shutil
from pyproj import Proj, transform
import glob
import random

from math import sqrt
import json


def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise

datasets_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/DATASETS"
mountain_path =os.path.join(datasets_path,"SUPPORT_DATA/DATA/mountains/m_massifs_v1_WGS84.shp")
images_path = os.path.join(datasets_path,"IMAGES")
satellite_path = os.path.join(images_path,"SPOT/TEST")
out_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/theia_download/coverage_output"
mkdir_p(out_path)
os.system("rm -f "+os.path.join(out_path,"*"))
where_burn= "\"m_massive='pyr'\"" 

if len(os.listdir(satellite_path)) == 0 : exit()


#############init shapefile
# set up the shapefile driver
driver = ogr.GetDriverByName("ESRI Shapefile")

# create the data source
data_source = driver.CreateDataSource(os.path.join(out_path,"theia_shp.shp"))

# create the spatial reference, WGS84
srs = osr.SpatialReference()
srs.ImportFromEPSG(4326)

# create the layer
layer = data_source.CreateLayer("theia_shp", srs, ogr.wkbPolygon)

# Add the fields we're interested in
a = ogr.FieldDefn("Location", ogr.OFTString)
a.SetWidth(24)
layer.CreateField(a)
b= ogr.FieldDefn("Collection", ogr.OFTString)
b.SetWidth(24)
layer.CreateField(b)
c = ogr.FieldDefn("Platform", ogr.OFTString)
c.SetWidth(24)
layer.CreateField(c)
layer.CreateField(ogr.FieldDefn("Nb_visit", ogr.OFTInteger))


#################start iterating


for location in os.listdir(satellite_path):
    print(location)

    if len(os.listdir(os.path.join(satellite_path,location))) == 0 : continue
    for collection in os.listdir(os.path.join(satellite_path,location)):
        print(collection)
        
        if len(os.listdir(os.path.join(satellite_path,location,collection))) == 0 : continue
        for platform in os.listdir(os.path.join(satellite_path,location,collection)):
            print(platform)
            
            platform_path = os.path.join(satellite_path,location,collection,platform)
            searchjson = os.path.join(platform_path,"search.json")
            if os.path.exists(searchjson) is False: continue
            with open(searchjson) as search_file:
                data = json.load(search_file)
                print(len(data["features"]))
                if len(data["features"]) == 0 : continue
                for i in range(len(data["features"])):
                    print("read product")
                    product = data["features"][i]
                    size_x = int(product["properties"]["nb_cols"]/5)
                    size_y = int(product["properties"]["nb_rows"]/5)
                    #resolution = product["properties"]["resolution"]
                    date = product["properties"]["startDate"] #SWH : "YYYY-MM-DDTHH:mm:ssZ"
                    coordinates = product["geometry"]["coordinates"] #SWH : [[[minx,maxy],[maxx,maxy],[maxx,miny],[minx,miny],[minx,maxy]]]
                    upperl = coordinates[0][0]
                    upperr = coordinates[0][1]
                    lowerr = coordinates[0][2]
                    lowerl = coordinates[0][3]
                    xmin = lowerl[0]
                    xmax = upperr[0]
                    ymax = upperr[1]
                    ymin = lowerl[1]
                    xres_platform = float(xmax - xmin)/float(size_x)
                    yres_platform = float(ymax - ymin)/float(size_y)
                    #if xres_platform not in xres: xres.append(xres_platform)
                    #if yres_platform not in xres: yres.append(xres_platform)


                    print("add field values")
                    # create the feature
                    feature = ogr.Feature(layer.GetLayerDefn())
                    # Set the attributes using the values from the delimited text file
                    print("add loc")
                    feature.SetField("Location", location)
                    print("add col")
                    feature.SetField("Collection", collection)
                    print("add plat")
                    feature.SetField("Platform", platform)
                    print("add nb")
                    feature.SetField("Nb_visit", 1)


                    # create the WKT for the feature using Python string formatting
                    print("add coords")
                    wkt = "POLYGON((%f %f,%f %f,%f %f,%f %f))" %  (upperl[0], upperl[1],upperr[0], upperr[1],lowerr[0], lowerr[1],lowerl[0], lowerl[1])

                    # Create the point from the Well Known Txt
                    point = ogr.CreateGeometryFromWkt(wkt)

                    # Set the feature geometry using the point
                    feature.SetGeometry(point)
                    # Create the feature in the layer (shapefile)
                    layer.CreateFeature(feature)
                    # Dereference the feature
                    feature = None





# Save and close the data source
data_source = None







