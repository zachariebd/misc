#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
import glob
import json
import time
import os
import os.path
import optparse
import sys
import job_organiser
import time

def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise



###############CRITERIA############################################





periods = []
periods.append(["1986-01-01","1987-12-31"])
periods.append(["1988-01-01","1989-12-31"])
periods.append(["1990-01-01","1991-12-31"])
periods.append(["1992-01-01","1993-12-31"])
periods.append(["1994-01-01","1995-12-31"])
periods.append(["1996-01-01","1997-12-31"])
periods.append(["1998-01-01","1999-12-31"])

periods.append(["2000-01-01","2001-12-31"])
periods.append(["2002-01-01","2003-12-31"])

periods.append(["2004-01-01","2005-12-31"])
periods.append(["2006-01-01","2007-12-31"])
periods.append(["2008-01-01","2009-12-31"])
periods.append(["2010-01-01","2011-12-31"])

periods.append(["2012-01-01","2013-12-31"])
periods.append(["2014-01-01","2015-12-31"])
periods.append(["2016-01-01","2017-12-31"])
periods.append(["2018-01-01","2019-12-31"])



locations={}
locations["coordinates"] = []  #[["Place",lonmin,lonmax,latmin,latmax]]
locations["coordinates"].append(["Pyrennees",-2,3.5,42,43.5]) 
locations["coordinates"].append(["Alps",4.7,8.4,43,49]) 
locations["coordinates"].append(["Massif",0.9,5.1,43.2,47.6]) 
locations["locations"] = []
locations["sites"] = []

satellites={}
satellites["SPOTWORLDHERITAGE"] = ["SPOT1","SPOT2","SPOT3","SPOT4","SPOT5"]
satellites["SWH1"] = ["SPOT1","SPOT2","SPOT3","SPOT4","SPOT5"]
#satellites["SWH1"] = ["SPOT1","SPOT2","SPOT4","SPOT5"]
#satellites["Landsat57"] = ["LANDSAT5","LANDSAT7","LANDSAT8"]

download = 0

theia_download_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/theia_download"
theia_products_path = '/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/DATASETS/IMAGES/SPOT'
theia_download_script = os.path.join(theia_download_path,"theia_download.py")
config_file = os.path.join(theia_download_path,"config_theia.cfg")
sh_script = os.path.join(theia_download_path,"theia_download_job.sh")

params_paths = "theia_download_path=\""+theia_download_path+"\",theia_download_script=\""+theia_download_script+"\",config_file=\""+config_file+"\""
params_download = "download=\""+str(download)+"\""



for collection in satellites:
    print(collection)
    for platform in satellites[collection]:
        print(platform)

        for period in periods:
            print(period)
            date_start = period[0]
            date_end = period[1]
            params_dates = "date_start=\""+date_start+"\",date_end=\""+date_end+"\""
            for coords in locations["coordinates"]:
                print("coordinates",coords)
                name = coords[0]
                lonmin = coords[1]
                lonmax = coords[2]
                latmin = coords[3]
                latmax = coords[4]
                params_coords = "lonmin=\""+str(lonmin)+"\",lonmax=\""+str(lonmax)+"\",latmin=\""+str(latmin)+"\",latmax=\""+str(latmax)+"\""
                
                
                output_dir = os.path.join(theia_products_path,name,collection,platform,date_start)
                if os.path.exists(output_dir) and output_dir != "/home/ad/barrouz/":
                    print(platform + " data from " +collection+" collection sent to dir: " + output_dir)
                elif os.path.exists(output_dir) is False:
                    print("creating output directory: " + output_dir)
                    mkdir_p(output_dir)
                    print(platform + " data from " +collection+" collection sent to dir: " + output_dir)
                else :
                    print("output directory is home directory: processing stopped")
                    continue
                params_out = "collection=\""+collection+"\",platform=\""+platform+"\",output_dir=\""+output_dir+"\""
                
                
                
                

                
                params = params_paths +","+params_download+","+params_out+","+params_dates+","+params_coords
                job_id = job_organiser.makeJobs(params,sh_script,platform+"_coordinates")
                time.sleep(1)
            for location in locations["locations"]:
                print("location",location)
                params_coords = "location=\""+location+"\""
                
                output_dir = os.path.join(theia_products_path,location,collection,platform)
                if os.path.exists(output_dir) and output_dir != "/home/ad/barrouz/":
                    print(platform + " data from " +collection+" collection sent to dir: " + output_dir)
                elif os.path.exists(output_dir) is False:
                    print("creating output directory: " + output_dir)
                    mkdir_p(output_dir)
                    print(platform + " data from " +collection+" collection sent to dir: " + output_dir)
                else :
                    print("output directory is home directory: processing stopped")
                    continue
                params_out = "collection=\""+collection+"\",platform=\""+platform+"\",output_dir=\""+output_dir+"\""

                
                params = params_paths +","+params_download+","+params_out+","+params_dates+","+params_coords
                job_id = job_organiser.makeJobs(params,sh_script,platform+"_"+location)
                time.sleep(1)
            for site in locations["sites"]:
                print("site",site)
                params_coords = "site=\""+site+"\""
                
                
                output_dir = os.path.join(theia_products_path,site,collection,platform)
                if os.path.exists(output_dir) and output_dir != "/home/ad/barrouz/":
                    print(platform + " data from " +collection+" collection sent to dir: " + output_dir)
                elif os.path.exists(output_dir) is False:
                    print("creating output directory: " + output_dir)
                    mkdir_p(output_dir)
                    print(platform + " data from " +collection+" collection sent to dir: " + output_dir)
                else :
                    print("output directory is home directory: processing stopped")
                    continue
                params_out = "collection=\""+collection+"\",platform=\""+platform+"\",output_dir=\""+output_dir+"\""        

                
                params = params_paths +","+params_download+","+params_out+","+params_dates+","+params_coords
                job_id = job_organiser.makeJobs(params,sh_script,platform+"_"+site)
                time.sleep(1)











