#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
from osgeo import osr, ogr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import shutil
from pyproj import Proj, transform
import glob
import random

from math import sqrt
import json


def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise

datasets_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/DATASETS"
mountain_path =os.path.join(datasets_path,"SUPPORT_DATA/DATA/mountains/m_massifs_v1_WGS84.shp")
images_path = os.path.join(datasets_path,"IMAGES")
satellite_path = os.path.join(images_path,"SPOT/TEST")
out_path = "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/theia_download/coverage_output"
mkdir_p(out_path)
os.system("rm -f "+os.path.join(out_path,"*"))
where_burn= "\"m_massive='pyr'\"" 

xres = []
yres = []
if len(os.listdir(satellite_path)) == 0 : exit()
for location in os.listdir(satellite_path):
    print(location)
    xmin_location= 0
    xmax_location= 0
    ymax_location= 0
    ymin_location= 0
    nb_collection = 0

    if len(os.listdir(os.path.join(satellite_path,location))) == 0 : continue
    for collection in os.listdir(os.path.join(satellite_path,location)):
        print(collection)
        xmin_collection= 0
        xmax_collection= 0
        ymax_collection= 0
        ymin_collection= 0
        nb_platform = 0
        
        if len(os.listdir(os.path.join(satellite_path,location,collection))) == 0 : continue
        for platform in os.listdir(os.path.join(satellite_path,location,collection)):
            print(platform)
            
            platform_path = os.path.join(satellite_path,location,collection,platform)
            searchjson = os.path.join(platform_path,"search.json")
            if os.path.exists(searchjson) is False: continue
            os.system("rm -f "+os.path.join(out_path,"temp_*.tif"))
            xmin_platform= 0
            xmax_platform= 0
            ymax_platform= 0
            ymin_platform= 0
            xres_platform = 0
            yres_platform = 0
            with open(searchjson) as search_file:
                data = json.load(search_file)
                print(len(data["features"]))
                if len(data["features"]) == 0 : continue
                for i in range(len(data["features"])):
                    product = data["features"][i]
                    size_x = int(product["properties"]["nb_cols"]/5)
                    size_y = int(product["properties"]["nb_rows"]/5)
                    #resolution = product["properties"]["resolution"]
                    date = product["properties"]["startDate"] #SWH : "YYYY-MM-DDTHH:mm:ssZ"
                    print(date)
                    coordinates = product["geometry"]["coordinates"] #SWH : [[[minx,maxy],[maxx,maxy],[maxx,miny],[minx,miny],[minx,maxy]]]
                    upperl = coordinates[0][0]
                    upperr = coordinates[0][1]
                    lowerr = coordinates[0][2]
                    lowerl = coordinates[0][3]
                    xmin = lowerl[0]
                    xmax = upperr[0]
                    ymax = upperr[1]
                    ymin = lowerl[1]
                    xres_platform = float(xmax - xmin)/float(size_x)
                    yres_platform = float(ymax - ymin)/float(size_y)
                    #if xres_platform not in xres: xres.append(xres_platform)
                    #if yres_platform not in xres: yres.append(xres_platform)
                    
                    ####gdal
                    fileformat = "GTiff"
                    driver = gdal.GetDriverByName(fileformat)
                    dst_ds = driver.Create(os.path.join(out_path,"temp_"+str(i)+".tif"),xsize=size_x,ysize=size_y,bands=1,eType=gdal.GDT_UInt16)
                    dst_ds.SetGeoTransform([xmin,xres_platform,0,ymax,0,-1*yres_platform])
                    srs = osr.SpatialReference()       
                    srs.ImportFromEPSG(4326) 
                    dst_ds.SetProjection(srs.ExportToWkt()) 
                    raster = numpy.ones((size_y, size_x), dtype=numpy.uint16)
                    dst_ds.GetRasterBand(1).WriteArray(raster)
                    dst_ds = None
                    
                    if i == 0:
                        xmin_platform= xmin
                        xmax_platform= xmax
                        ymax_platform= ymax
                        ymin_platform= ymin
                    else:
                        if xmin < xmin_platform: xmin_platform = xmin
                        if xmax > xmax_platform: xmax_platform = xmax
                        if ymax > ymax_platform: ymax_platform = ymax
                        if ymin < ymin_platform: ymin_platform = ymin
                
            print("generate A")
            platform_stacked = os.path.join(out_path,location+"_"+collection+"_"+platform+"_"+"stacked.vrt")
            platform_sum = os.path.join(out_path,location+"_"+collection+"_"+platform+"_A-sum.tif")
            os.system("rm -f "+platform_sum+" "+platform_stacked)
            os.system("gdalbuildvrt -q -vrtnodata 0 -separate -te "+str(xmin_platform)+" "+str(ymin_platform)+" "+str(xmax_platform)+" "+str(ymax_platform)+" "+platform_stacked+" "+ os.path.join(out_path,"temp_*.tif"))
            
            g = gdal.Open(platform_stacked)
            data = g.ReadAsArray()
            if i > 1:
                asum = data.sum(axis=0)
            else :
                asum = data
            size_y = asum.shape[0]
            size_x = asum.shape[1]
            os.system("rm -f "+os.path.join(out_path,"temp_*.tif"))
            fileformat = "GTiff"
            driver = gdal.GetDriverByName(fileformat)
            dst_ds = driver.Create(platform_sum,xsize=size_x,ysize=size_y,bands=1,eType=gdal.GDT_UInt16)
            dst_ds.SetGeoTransform([xmin_platform,xres_platform,0,ymax_platform,0,-1*yres_platform])
            srs = osr.SpatialReference()       
            srs.ImportFromEPSG(4326) 
            dst_ds.SetProjection(srs.ExportToWkt()) 
            dst_ds.GetRasterBand(1).WriteArray(asum)
            dst_ds = None
                
                    
            if nb_platform == 0:
                xmin_collection= xmin_platform
                xmax_collection= xmax_platform
                ymax_collection= ymax_platform
                ymin_collection= ymin_platform
            else:
                if xmin_platform < xmin_collection: xmin_collection = xmin_platform
                if xmax_platform > xmax_collection: xmax_collection = xmax_platform
                if ymax_platform > ymax_collection: ymax_collection = ymax_platform
                if ymin_platform < ymin_collection: ymin_collection = ymin_platform
            nb_platform = nb_platform + 1     
        
        print("generate B")
        collection_stacked = os.path.join(out_path,location+"_"+collection+"_"+"stacked.vrt")
        collection_sum = os.path.join(out_path,location+"_"+collection+"_B-sum.tif")
        os.system("rm -f "+collection_sum+" "+collection_stacked)
        os.system("gdalbuildvrt -q -vrtnodata 0 -separate -te "+str(xmin_collection)+" "+str(ymin_collection)+" "+str(xmax_collection)+" "+str(ymax_collection)+" "+collection_stacked+" "+ os.path.join(out_path,location+"_"+collection+"_*_A-sum.tif"))
        g = gdal.Open(collection_stacked)
        data = g.ReadAsArray()
        if nb_platform > 1:
            asum = data.sum(axis=0)
        else :
            asum = data
        size_y = asum.shape[0]
        size_x = asum.shape[1]
        xres_collection = float(xmax_collection - xmin_collection)/float(size_x)
        yres_collection = float(ymax_collection - ymin_collection)/float(size_y)
        fileformat = "GTiff"
        driver = gdal.GetDriverByName(fileformat)
        dst_ds = driver.Create(collection_sum,xsize=size_x,ysize=size_y,bands=1,eType=gdal.GDT_UInt16)
        dst_ds.SetGeoTransform([xmin_collection,xres_collection,0,ymax_collection,0,-1*yres_collection])
        srs = osr.SpatialReference()       
        srs.ImportFromEPSG(4326) 
        dst_ds.SetProjection(srs.ExportToWkt()) 
        dst_ds.GetRasterBand(1).WriteArray(asum)
        dst_ds = None
                        
                        
        if nb_collection == 0:
            xmin_location= xmin_collection
            xmax_location= xmax_collection
            ymax_location= ymax_collection
            ymin_location= ymin_collection
        else:
            if xmin_collection < xmin_location: xmin_location = xmin_collection
            if xmax_collection > xmax_location: xmax_location = xmax_collection
            if ymax_collection > ymax_location: ymax_location = ymax_collection
            if ymin_collection < ymin_location: ymin_location = ymin_collection
        nb_collection = nb_collection + 1   

    print("generate C")
    location_stacked = os.path.join(out_path,location+"_"+"stacked.vrt")
    location_sum = os.path.join(out_path,location+"_C-sum.tif")
    os.system("rm -f "+location_sum+" "+location_stacked)
    os.system("gdalbuildvrt -q  -vrtnodata 0 -separate -te "+str(xmin_location)+" "+str(ymin_location)+" "+str(xmax_location)+" "+str(ymax_location)+" "+location_stacked+" "+ os.path.join(out_path,location+"_*_B-sum.tif"))
    g = gdal.Open(location_stacked)
    data = g.ReadAsArray()
    if nb_collection > 1:
        asum = data.sum(axis=0)
    else :
        asum = data
    size_y = asum.shape[0]
    size_x = asum.shape[1]
    xres_location = float(xmax_location - xmin_location)/float(size_x)
    yres_location = float(ymax_location - ymin_location)/float(size_y)
    fileformat = "GTiff"
    driver = gdal.GetDriverByName(fileformat)
    dst_ds = driver.Create(location_sum,xsize=size_x,ysize=size_y,bands=1,eType=gdal.GDT_UInt16)
    dst_ds.SetGeoTransform([xmin_location,xres_location,0,ymax_location,0,-1*yres_location])
    srs = osr.SpatialReference()       
    srs.ImportFromEPSG(4326) 
    dst_ds.SetProjection(srs.ExportToWkt()) 
    dst_ds.GetRasterBand(1).WriteArray(asum)
    dst_ds = None
    masked_location_sum = os.path.join(out_path,location+"_C-sum-masked.tif")
    os.system("cp "+location_sum+" "+masked_location_sum)
    os.system("gdal_rasterize -where "+where_burn+" -i  -burn 0 " + mountain_path + " " + masked_location_sum)
                    
            
