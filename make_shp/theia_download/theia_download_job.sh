#!/bin/bash
#PBS -j oe
#PBS -l select=1:ncpus=4:mem=20000mb
#PBS -l walltime=03:59:00




PBS_TMPDIR=$TMPDIR
# Unset TMPDIR env variable which conflicts with openmpi
unset TMPDIR

cd $output_dir

current=$PWD


if [ "$current" = "/home/ad/barrouz" ]; then
    echo "danger: home directory /home/ad/barrouz !"
    exit

fi

download_option=""

if [ "$download" -eq "0" ] ; then
    download_option="--no_download"
fi

echo "START OF THEIA DOWNLOAD"
echo $config_file

if [[ -v location ]]; then
    echo "location"
    python $theia_download_script -l $location -a $config_file -c $collection -p $platform -d $date_start -f $date_end ${download_option}

elif [[ -v site ]]; then
    echo "site"
    python $theia_download_script -s $site -a $config_file -c $collection -p $platform -d $date_start -f $date_end ${download_option}

elif [[ -v lonmin ]]; then
    echo "lonlat"
    
    python $theia_download_script --lonmin $lonmin --lonmax $lonmax --latmin $latmin --latmax $latmax -a $config_file -c $collection -p $platform -d $date_start -f $date_end ${download_option}
fi

#echo "UNZIP PRODUCTS"
#unzip *.zip
#echo "GIVE PERMISSIONS"
#chmod +rwx *.zip
#echo "REMOVE ZIP PRODUCTS"
#rm *.zip
