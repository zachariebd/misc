#!/bin/bash
#PBS -j oe
#PBS -l select=1:ncpus=4:mem=50000mb
#PBS -l walltime=03:59:00




PBS_TMPDIR=$TMPDIR
# Unset TMPDIR env variable which conflicts with openmpi
unset TMPDIR
cd /work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/theia_download
python /work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/get_products/theia_download/coverage.py
