import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
from osgeo import osr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.optimize as opti
from scipy.stats import mstats
import shutil
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
from pyproj import Proj, transform
import glob
import random
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
from matplotlib.ticker import PercentFormatter
from zoo_utils import *

tifs_dir = "TIFS"
mkdir_p(tifs_dir)
pngs_dir = "PNGs"
mkdir_p(pngs_dir)

tilesize = 50

input_tif = "IMG_L2A_TOA_20171124_a20161101T103346_pPHR1B_mMS.TIF"

output = "20171124"

g_input = gdal.Open(input_tif)

pSizeX = g_input.RasterXSize
pSizeY = g_input.RasterYSize
g_input = None

#POUR CHAQUE SEGMENT DE L'IMAGE
for i in range(0, 100, tilesize):
    for j in range(0, 100, tilesize):
        #ON PRODUIT LES TIFS DU SEGMENT
        tif_dir = os.path.join(tifs_dir,str(i)+"_"+str(j))
        mkdir_p(tif_dir)
        all_output_tif = os.path.join(tif_dir,str(i)+"_"+str(j)+"_all_"+output+".TIF")
        rgb_output_tif = os.path.join(tif_dir,str(i)+"_"+str(j)+"_rgb_"+output+".TIF")
        irg_output_tif = os.path.join(tif_dir,str(i)+"_"+str(j)+"_irg_"+output+".TIF")
        ndsi_output_tif = os.path.join(tif_dir,str(i)+"_"+str(j)+"_ndsi_"+output+".TIF")
        gt_all = "gdal_translate -of GTIFF -srcwin "+str(i)+", "+str(j)+", "+str(tilesize)+", " \
            +str(tilesize)+" "+input_tif+" "+all_output_tif
        os.system(gt_all)
        gt_rgb = "gdal_translate -of GTIFF -b 1 -b 2 -b 3 -srcwin "+str(i)+", "+str(j)+", "+str(tilesize)+", " \
            +str(tilesize)+" "+input_tif+" "+rgb_output_tif
        os.system(gt_rgb)
        gt_irg = "gdal_translate -of GTIFF -b 4 -b 1 -b 2 -srcwin "+str(i)+", "+str(j)+", "+str(tilesize)+", " \
            +str(tilesize)+" "+input_tif+" "+irg_output_tif
        os.system(gt_irg)
        gc_ndsi = "gdal_calc.py -A "+all_output_tif+" --A_band=2 -B "+all_output_tif+" --B_band=4 --calc=\"(A-B)/(A+B)\" --overwrite --outfile="+ndsi_output_tif
        os.system(gc_ndsi)
        #ON PRODUIT LES PNGS DU SEGMENT
        png_dir = os.path.join(pngs_dir,str(i)+"_"+str(j))
        all_output_png = os.path.join(png_dir,str(i)+"_"+str(j)+"_all_"+output+".png")
        rgb_output_png = os.path.join(png_dir,str(i)+"_"+str(j)+"_rgb_"+output+".png")
        irg_output_png = os.path.join(png_dir,str(i)+"_"+str(j)+"_irg_"+output+".png")
        ndsi_output_png = os.path.join(png_dir,str(i)+"_"+str(j)+"_ndsi_"+output+".png")
 
        gt_rgb = "gdal_translate -of PNG  "+rgb_output_tif+" "+rgb_output_png
        os.system(gt_rgb)
        gt_irg = "gdal_translate -of PNG    "+irg_output_tif+" "+irg_output_png
        os.system(gt_irg)
        gt_ndsi = "gdal_translate -of PNG   "+ndsi_output_tif+" "+ndsi_output_png
        os.system(gt_ndsi)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        


